import os
import unittest
import ast
import pickle

from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, column_parser
from tamu_primitives import loader_batching


class FeatureSelectionTestCase(unittest.TestCase):
    def _get_iris(self):
        dataset_doc_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))
    
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
    
        hyperparams_class = \
            dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
    
        dataframe = primitive.produce(inputs=dataset).value
    
        return dataframe

    def _get_iris_columns(self):
        dataframe = self._get_iris()
    
        # We set custom metadata on columns.
        for column_index in range(1, 5):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'attributes'})
        for column_index in range(5, 6):
            dataframe.metadata = dataframe.metadata.update_column(column_index, {'custom_metadata': 'targets'})
    
        # We set semantic types like runtime would.
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/Target')
        dataframe.metadata = dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                  'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        dataframe.metadata = dataframe.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5),
                                                                     'https://metadata.datadrivendiscovery.org/types/Attribute')
    
        # Parsing.
        hyperparams_class = \
            column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())
        dataframe = primitive.produce(inputs=dataframe).value
    
        hyperparams_class = \
            extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code'][
                'class_type_arguments']['Hyperparams']
    
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Attribute',)}))
        attributes = primitive.produce(inputs=dataframe).value
    
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(
            hyperparams=hyperparams_class.defaults().replace(
                {'semantic_types': ('https://metadata.datadrivendiscovery.org/types/SuggestedTarget',)}))
        targets = primitive.produce(inputs=dataframe).value
    
        return dataframe, attributes, targets

    def test_basic(self):
        dataframe, attributes, targets = self._get_iris_columns()

        self.assertEqual(list(targets.columns), ['species'])
        hyperparams_class = \
            loader_batching.LoaderBatchingPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = loader_batching.LoaderBatchingPrimitive(hyperparams=hyperparams_class.defaults())
        primitive.set_training_data(inputs=attributes, outputs=targets)
        primitive.fit()
    
        predictions = primitive.produce(inputs=attributes).value

        self.assertEqual(predictions.shape[0], 150)
        self.assertEqual(len(predictions.columns), 1)
        self.assertEqual(predictions.metadata.query_column(0)['name'], 'predictions')

    def test_pickle(self):
        ataframe, attributes, targets = self._get_iris_columns()

        self.assertEqual(list(targets.columns), ['species'])
        hyperparams_class = \
            loader_batching.LoaderBatchingPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = loader_batching.LoaderBatchingPrimitive(hyperparams=hyperparams_class.defaults())
        primitive.set_training_data(inputs=attributes, outputs=targets)
        primitive.fit()

        str_primitive_pickle = str(pickle.dumps(primitive))
        unpickle_primitive = pickle.loads(ast.literal_eval(str_primitive_pickle))

        predictions = unpickle_primitive.produce(inputs=attributes).value

        self.assertEqual(predictions.shape[0], 150)
        self.assertEqual(len(predictions.columns), 1)
        self.assertEqual(predictions.metadata.query_column(0)['name'], 'predictions')

    def test_stratified(self):
        dataframe, attributes, targets = self._get_iris_columns()

        self.assertEqual(list(targets.columns), ['species'])
        hyperparams_class = \
            loader_batching.LoaderBatchingPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = loader_batching.LoaderBatchingPrimitive(hyperparams=hyperparams_class.defaults().replace(
            {'sampling_method': 'stratified'}))
        primitive.set_training_data(inputs=attributes, outputs=targets)
        primitive.fit()

        predictions = primitive.produce(inputs=attributes).value

        self.assertEqual(predictions.shape[0], 150)
        self.assertEqual(len(predictions.columns), 1)
        self.assertEqual(predictions.metadata.query_column(0)['name'], 'predictions')

    def test_random(self):
        dataframe, attributes, targets = self._get_iris_columns()

        self.assertEqual(list(targets.columns), ['species'])
        hyperparams_class = \
            loader_batching.LoaderBatchingPrimitive.metadata.query()['primitive_code']['class_type_arguments'][
                'Hyperparams']
        primitive = loader_batching.LoaderBatchingPrimitive(hyperparams=hyperparams_class.defaults().replace(
            {'sampling_method': 'random'}))
        primitive.set_training_data(inputs=attributes, outputs=targets)
        primitive.fit()

        predictions = primitive.produce(inputs=attributes).value

        self.assertEqual(predictions.shape[0], 150)
        self.assertEqual(len(predictions.columns), 1)
        self.assertEqual(predictions.metadata.query_column(0)['name'], 'predictions')


if __name__ == '__main__':
    unittest.main()