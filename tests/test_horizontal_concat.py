import unittest
import pandas as pd

import numpy

from d3m import container
from d3m.metadata import base as metadata_base

from tamu_primitives import horizontal_concat


class HorizontalConcatPrimitiveTestCase(unittest.TestCase):

    # No indexes
    def test_basic(self):
        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = container.DataFrame(data=test_data_inputs, generate_metadata=True)

        test_data_targets = {'col2': [1, 2 ,3]}
        dataframe_targets = container.DataFrame(data=test_data_targets, generate_metadata=True)

        hyperparams_class = horizontal_concat.HorizontalConcatListPrimitive.metadata.get_hyperparams()
        primitive = horizontal_concat.HorizontalConcatListPrimitive(hyperparams=hyperparams_class.defaults())
        call_result = primitive.produce(inputs=[dataframe_inputs, dataframe_targets])

        dataframe_concat = call_result.value
        self.assertEqual(dataframe_concat.values.tolist(), [[1.0, 1.0], [2.0, 2.0], [3.0, 3.0]])
        self._test_basic_metadata(dataframe_concat.metadata)

    def _test_basic_metadata(self, metadata):
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 2)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['name'], 'col1')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['structural_type'], numpy.float64)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['name'], 'col2')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['structural_type'], numpy.int64)

    # Test with one index
    def test_single_index(self):
        # Case for the index on the first dataframe.
        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = generate_dataframe(test_data_inputs, index=True)

        test_data_targets = {'col2': [1, 2, 3]}
        dataframe_targets = container.DataFrame(data=test_data_targets, generate_metadata=True)

        hyperparams_class = horizontal_concat.HorizontalConcatListPrimitive.metadata.get_hyperparams()
        primitive = horizontal_concat.HorizontalConcatListPrimitive(hyperparams=hyperparams_class.defaults())
        call_result = primitive.produce(inputs=[dataframe_inputs, dataframe_targets])

        dataframe_concat = call_result.value

        self.assertEqual(dataframe_concat.values.tolist(), [[0, 1.0, 1.0], [1, 2.0, 2.0], [2, 3.0, 3.0]])
        self._test_index_metadata(dataframe_concat.metadata)

        # Case for the index not on the fist dataframe.
        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = container.DataFrame(data=test_data_inputs, generate_metadata=True)

        test_data_targets = {'col2': [1, 2, 3]}
        dataframe_targets = generate_dataframe(test_data_targets, index=True)

        hyperparams_class = horizontal_concat.HorizontalConcatListPrimitive.metadata.get_hyperparams()
        primitive = horizontal_concat.HorizontalConcatListPrimitive(hyperparams=hyperparams_class.defaults())
        call_result = primitive.produce(inputs=[dataframe_targets, dataframe_inputs])

        dataframe_concat = call_result.value

        self.assertEqual(dataframe_concat.values.tolist(), [[0, 1.0, 1.0], [1, 2.0, 2.0], [2, 3.0, 3.0]])
        self._test_second_index_metadata(dataframe_concat.metadata)

    def _test_index_metadata(self, metadata):
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 3)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['name'], 'd3mIndex')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['structural_type'], numpy.int64)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['semantic_types'], (
            'http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
        ))
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['name'], 'col1')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['structural_type'], numpy.float64)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 2))['name'], 'col2')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 2))['structural_type'], numpy.int64)

    def _test_second_index_metadata(self, metadata):
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 3)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['name'], 'd3mIndex')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['structural_type'], numpy.int64)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 0))['semantic_types'], (
            'http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
        ))
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 2))['name'], 'col1')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 2))['structural_type'], numpy.float64)
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['name'], 'col2')
        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS, 1))['structural_type'], numpy.int64)

    # Test with one index
    def test_multiple_index(self):
        # Case for the index on the first dataframe.
        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = generate_dataframe(test_data_inputs, index=True)

        test_data_targets = {'col2': [1, 2, 3]}
        dataframe_targets = generate_dataframe(test_data_targets, index=True)

        hyperparams_class = horizontal_concat.HorizontalConcatListPrimitive.metadata.get_hyperparams()
        primitive = horizontal_concat.HorizontalConcatListPrimitive(hyperparams=hyperparams_class.defaults())
        call_result = primitive.produce(inputs=[dataframe_inputs, dataframe_targets])

        dataframe_concat = call_result.value

        self.assertEqual(dataframe_concat.values.tolist(), [[0, 1.0, 1.0], [1, 2.0, 2.0], [2, 3.0, 3.0]])
        self._test_index_metadata(dataframe_concat.metadata)


def generate_dataframe(data, index=False):
    dataframe = pd.DataFrame(data)

    if index:
        dataframe.insert(0, 'd3mIndex', range(len(dataframe)))

    dataframe = container.DataFrame(data=dataframe, generate_metadata=True)

    if index:
        dataframe.metadata = dataframe.metadata.update(
            (metadata_base.ALL_ELEMENTS, 0),
            {
                'name': 'd3mIndex',
                'structural_type': numpy.int64,
                'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })
    return dataframe


if __name__ == '__main__':
    unittest.main()
