import unittest
import pandas as pd

import numpy

from d3m import container
from d3m.metadata import base as metadata_base

from tamu_primitives import dataframe_list_extractor


class HorizontalConcatPrimitiveTestCase(unittest.TestCase):

    # No indexes
    def test_basic(self):
        dataframe_list = container.List()
        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = container.DataFrame(data=test_data_inputs, generate_metadata=True)
        dataframe_list.append(dataframe_inputs)

        test_data_targets = {'col2': [1, 2 ,3]}
        dataframe_targets = container.DataFrame(data=test_data_targets, generate_metadata=True)
        dataframe_list.append(dataframe_targets)

        hyperparams_class = dataframe_list_extractor.DataframeListExtractorPrimitive.metadata.get_hyperparams()
        primitive = dataframe_list_extractor.DataframeListExtractorPrimitive(hyperparams=hyperparams_class.defaults())
        call_result = primitive.produce(inputs=dataframe_list)

        self.assertEqual(call_result.value.metadata, dataframe_inputs.metadata)
        self.assertEqual(call_result.value.values.tolist(), dataframe_inputs.values.tolist())

        hyperparams_class = dataframe_list_extractor.DataframeListExtractorPrimitive.metadata.get_hyperparams()
        primitive = dataframe_list_extractor.DataframeListExtractorPrimitive(hyperparams=hyperparams_class.defaults().replace({'dataframe_to_extract': 1}))
        call_result = primitive.produce(inputs=dataframe_list)

        self.assertEqual(call_result.value.metadata, dataframe_targets.metadata)
        self.assertEqual(call_result.value.values.tolist(), dataframe_targets.values.tolist())



def generate_dataframe(data, index=False):
    dataframe = pd.DataFrame(data)

    if index:
        dataframe.insert(0, 'd3mIndex', range(len(dataframe)))

    dataframe = container.DataFrame(data=dataframe, generate_metadata=True)

    if index:
        dataframe.metadata = dataframe.metadata.update(
            (metadata_base.ALL_ELEMENTS, 0),
            {
                'name': 'd3mIndex',
                'structural_type': numpy.int64,
                'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })
    return dataframe


if __name__ == '__main__':
    unittest.main()
