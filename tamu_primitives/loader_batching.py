import os
import math
import random

import d3m.metadata.base as metadata_module
from d3m import container, utils as d3m_utils
from d3m.metadata import hyperparams, params
from d3m.primitive_interfaces import base
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from test_primitives.null import (NullDataFrameUnsupervisedLearnerPrimitive as NullDataframePrimitive,
                                  Hyperparams as NullDataFrameUnsuperviseHyperparameters)
from test_primitives.random_classifier import RandomClassifierPrimitive, Hyperparams as RandomClassifierPrimitiveHyperpameters
from d3m.primitive_interfaces.base import CallResult, ContinueFitMixin
from sklearn.model_selection import StratifiedShuffleSplit

import tamu_primitives

Inputs = container.DataFrame
Outputs = container.DataFrame


def get_column_by_semantic_type(input_dataframe, semantic_type):
    columns_ids = []
    for i in range(len(input_dataframe.columns)):
        column_semantic_types = input_dataframe.metadata.query((metadata_module.ALL_ELEMENTS, i))['semantic_types']
        if semantic_type in column_semantic_types:
            columns_ids.append(i)
    if not columns_ids:
        raise ValueError("No columns with semantic_types {} where found".format(semantic_type))
    output_columns = input_dataframe.iloc[:, columns_ids]
    return output_columns


class Hyperparams(hyperparams.Hyperparams):
    primitive_reader = hyperparams.Hyperparameter[base.PrimitiveBase](
        default=NullDataframePrimitive(hyperparams=NullDataFrameUnsuperviseHyperparameters.defaults()),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='The primitive instance that is going to be use to load the data, such as an image reader. \
        The primitive must not fit like a transformer.'
    )
    primitive_learner = hyperparams.Hyperparameter[base.PrimitiveBase](
        default=RandomClassifierPrimitive(hyperparams=RandomClassifierPrimitiveHyperpameters.defaults()),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='The primitive instance to be trained. This primitive must implement continue_fit.'
    )
    batch_size = hyperparams.Bounded[int](
        default=10,
        lower=1,
        upper=None,
        description='Number of samples to be use for every batch',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

    sampling_method = hyperparams.Enumeration(
        values=['stratified', 'random', 'order'],
        default='stratified',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description="different sampling method to choose during batching")

    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. /"
                    "Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )


class Params(params.Params):
    _hyperparameters: Hyperparams
    _fitted: bool
    _random_seed: int


class LoaderBatchingPrimitive(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A collection of Feature selection methods wrapped from SKfeatures.
    """

    __author__ = "TAMU DARPA D3M Team, Diego Martinez <dmartinez05@tamu.edu>"
    metadata = metadata_module.PrimitiveMetadata({
        'id': 'd82c1549-4b58-4ef1-a7e4-1c2f441a1256',
        "version": "0.1.0",
        "name": "Batching reader",
        "source": {
            'name': tamu_primitives.__author__,
            'contact': 'mailto:dmartinez04@tamu.edu',
            'uris': [
                'https://gitlab.com/TAMU_D3M/d3m_primitives/blob/master/tamu_primitives/loader_batching.py',
                'https://gitlab.com/TAMU_D3M/d3m_primitives.git',
            ],
        },
        "installation": [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/TAMU_D3M/d3m_primitives.git@{git_commit}#egg=tamu_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        "python_path": "d3m.primitives.data_wrangling.batching.TAMU",
        "algorithm_types": [metadata_module.PrimitiveAlgorithmType.ADAPTIVE_ALGORITHM],
        "primitive_family": "DATA_WRANGLING",
    })

    def __init__(self, *, hyperparams: Hyperparams, random_seed: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        if not isinstance(self.hyperparams['primitive_learner'], ContinueFitMixin):
            raise ValueError("Primitivie {} does not implement continue_fit")

        self._fitted: bool = False
        self._training_inputs: Inputs = None
        self._training_outputs: Outputs = None
        self.random_seed = random_seed

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        if self.hyperparams['use_semantic_types'] is False:
            self._training_outputs = outputs
        self._fitted = False

    def _set_learner_chunk_training_data(self, lower_bound, upper_boud):
        _read_input_chunk = self.hyperparams['primitive_reader'].produce(
            inputs=self._training_inputs[lower_bound: upper_boud].reset_index(drop=True)).value

        if self.hyperparams['use_semantic_types']:
            self.hyperparams['primitive_learner'].set_training_data(inputs=_read_input_chunk)
        else:
            _output_chunk = self._training_outputs[lower_bound: upper_boud].reset_index(drop=True)
            self.hyperparams['primitive_learner'].set_training_data(inputs=_read_input_chunk, outputs=_output_chunk)

    def _generate_stratified_samples(self, batch_size):
        if self._training_outputs is None:
            output_columns = get_column_by_semantic_type(self._training_inputs, 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        else:
            output_columns = self._training_outputs

        n_folds = math.ceil(len(self._training_inputs)/batch_size)
        train_size = batch_size/len(self._training_inputs)
        stratified_kfold = StratifiedShuffleSplit(n_splits=n_folds, train_size=train_size, random_state=self.random_seed)
        splits = stratified_kfold.split(self._training_inputs, output_columns)
        return splits

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        batch_size = self.hyperparams['batch_size']

        if self.hyperparams['use_semantic_types']:
            get_unique_targets(self._training_inputs)
        else:
            get_unique_targets(self._training_outputs)
            self._training_outputs.metadata.pretty_print()

        # if not stratified, just do the normal, by going though chuncks until no more data left
        if self.hyperparams['sampling_method'] == 'order':
            # We need to set training data and fit before start calling continue_fir
            self._set_learner_chunk_training_data(0, batch_size)
            self.hyperparams['primitive_learner'].fit()

            for i_chunk in range(batch_size, len(self._training_inputs), batch_size):
                self._set_learner_chunk_training_data(i_chunk, i_chunk + batch_size)
                # fit on the current data
                self.hyperparams['primitive_learner'].continue_fit()

        elif self.hyperparams['sampling_method'] == 'stratified':
            # if stratified the batch size is going to be determine by math.ceil(n_samples/batch_size) so we can do
            # stratified sampling.
            learner_fitted = False
            for sample_indexes, _ in self._generate_stratified_samples(batch_size):
                self._set_training_data_with_indexes(sample_indexes)
                learner_fitted = self._fit_continue_fit(learner_fitted)

        elif self.hyperparams['sampling_method'] == 'random':
            # for this case we create a list with indexes, we shuffle them and iterate over them.
            random.seed(self.random_seed)
            indexes = list(range(len(self._training_inputs)))
            random.shuffle(indexes)

            learner_fitted = False
            for i_chunk in range(0, len(self._training_inputs), batch_size):
                if i_chunk + batch_size > len(self._training_inputs):
                    upper_bound = len(self._training_inputs)
                else:
                    upper_bound = i_chunk + batch_size

                sample_indexes = indexes[i_chunk:upper_bound]
                self._set_training_data_with_indexes(sample_indexes)
                learner_fitted = self._fit_continue_fit(learner_fitted)
        else:
            raise ValueError('Sampling method {} not supported'.format(self.hyperparams['sampling_method']))

        self._fitted = True
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        batch_size = self.hyperparams['batch_size']
        outputs = None
        for i_chunk in range(0, len(inputs), batch_size):
            # load the data
            _read_input_chunk = self.hyperparams['primitive_reader'].produce(
                inputs=inputs[i_chunk: i_chunk + batch_size].reset_index(drop=True)).value

            # get a partial result
            _partial_result = self.hyperparams['primitive_learner'].produce(inputs=_read_input_chunk).value

            if outputs is None:
                outputs = _partial_result
            else:
                outputs = outputs.append(_partial_result, ignore_index=True)

        # update the metadata with all the rows.
        outputs.metadata = outputs.metadata.update((), {'dimension': {'length': len(outputs)}})
        return CallResult(outputs)

    def _fit_continue_fit(self, learner_fitted):
        if not learner_fitted:
            self.hyperparams['primitive_learner'].fit()
        else:
            self.hyperparams['primitive_learner'].continue_fit()
        return True

    def _set_training_data_with_indexes(self, sample_indexes):
        train_inputs = self._training_inputs.iloc[sample_indexes].reset_index(drop=True)
        train_inputs = self.hyperparams['primitive_reader'].produce(inputs=train_inputs).value

        if self.hyperparams['use_semantic_types']:
            self.hyperparams['primitive_learner'].set_training_data(inputs=train_inputs)
        else:
            train_outputs = self._training_outputs.iloc[sample_indexes].reset_index(drop=True)
            self.hyperparams['primitive_learner'].set_training_data(inputs=train_inputs, outputs=train_outputs)

    def set_params(self, *, params: Params) -> None:
        self.hyperparams = params['_hyperparameters']
        self._fitted = params['_fitted']
        self.random_seed = params['_random_seed']

    def get_params(self) -> Params:
        return Params(
            _hyperparameters=self.hyperparams, _fitted=self._fitted, _random_seed=self.random_seed
        )


def get_unique_targets(data):
    output_columns = get_column_by_semantic_type(data, 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
    new_metadata = data.metadata
    for column_name in output_columns:
        index = list(data.columns).index(column_name)
        unique_targets = list(set(data[column_name].unique().tolist()))
        new_metadata = new_metadata.update((metadata_module.ALL_ELEMENTS, index,),
                                           {'all_distinct_values': unique_targets, })
    data.metadata = new_metadata
